package ru.tae.model;


public class JBFile {

    private int id;
    private String name;
    private String type;
    private String localPath;
    private int storageId;

    public JBFile()
    {
        this.id = 0;
        this.name = null;
        this.type = null;
        this.localPath = null;
        this.storageId = 0;
    }

    public JBFile(int id, String name, String type, String localPath) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.localPath = localPath;
        this.storageId =-1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public int getStorageId() {
        return storageId;
    }

    public void setStorageId(int storageId) {
        this.storageId = storageId;
    }

}
