package ru.tae.model;


import java.util.ArrayList;




public class JBLocalStorage implements IStorage {

    private int id;
    private String path;
    private StorageType storageType;
    private ArrayList<JBFile> files;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }


    public JBLocalStorage(int id, String path, StorageType storageType) {
        this.id = id;
        this.path = path;
        this.storageType = storageType;
        this.files = new ArrayList<>();
    }

    @Override
    public void generateStorage() {
        System.out.println("generating storage in "+this.path);
    }
    @Override
    public void uploadFile(JBFile file) {
        System.out.println("uploading file "+file.getName()+"."+file.getType()+" from "+file.getLocalPath()+" to "+this.path+"/"+file.getName());
        file.setStorageId(this.id);
        files.add(file);
    }

    @Override
    public void restoreFile(String path, JBFile file) {
        System.out.println("restoring file "+file.getName()+file.getType()+" from "+this.path+"/"+file.getName()+" to "+path);

    }

    @Override
    public ArrayList<JBFile> getList(){
        return this.files;
    }

    @Override
    public void restoreFile(JBFile file) {
        System.out.println("restoring file "+file.getName()+file.getType()+" from "+this.path+"/"+file.getName()+" to "+file.getLocalPath());
    }

    @Override
    public void updateFile(JBFile file) {
        System.out.println("updating file "+file.getName()+" ");
    }
}

