package ru.tae.model;

import java.util.ArrayList;

public interface IStorage {
    enum StorageType
    {
        LOCAL,
        YANDEX_DISK,
        DROPBOX
    }
    void generateStorage();
    void uploadFile(JBFile file);
    String getPath();
    void restoreFile(String path, JBFile file);
    void restoreFile(JBFile file);
    void updateFile(JBFile file);
    ArrayList<JBFile> getList();
}
