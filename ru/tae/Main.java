package ru.tae;

import ru.tae.model.IStorage;
import ru.tae.model.JBFile;
import ru.tae.model.JBLocalStorage;


import java.util.ArrayList;

public class Main {

    public static ArrayList<IStorage> storages = new ArrayList<>();
    public static void main(String[] args) {
        storages.add(new JBLocalStorage(0, "~/home/JustBackup", IStorage.StorageType.LOCAL));
        storages.get(0).generateStorage();
        for(int i=0; i<20; i++)
        {
            storages.get(0).uploadFile(new JBFile(i, "name"+i, "type", "~/"));
        }

        for(JBFile file : storages.get(0).getList())
        {
            System.out.println(file.getName()+"."+file.getType()+"  in directory "+storages.get(0).getPath());
        }

    }
}
